﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveAndBack : MonoBehaviour
{
    private int m_result = 0;
    public int SetResult
    {
        set => m_result = value;
    }

    public void OnClickSave()
    {
        if (m_result > PlayerPrefs.GetInt("Stickers"))
        {
            PlayerPrefs.SetInt("Stickers", m_result);
        }
        
        SceneManager.LoadSceneAsync(0);
    }
}
