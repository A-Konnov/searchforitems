﻿using System.IO;
using System.Collections;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class WebCam : MonoBehaviour
{
    public RawImage background;
    public RenderTexture renderScreen;
    public RenderTexture renderMarker;
    public RenderTexture renderMk1;
    public RenderTexture renderMk2;
    public GameObject loadPanel;

    public Text text;

    [SerializeField] List<Camera> columnCamera;
    [SerializeField] List<Camera> rowCamera;

    [SerializeField] List<RenderTexture> renderColumn = new List<RenderTexture>();
    [SerializeField] List<RenderTexture> renderRow = new List<RenderTexture>();

    static List<Texture2D> listTextureColumn = new List<Texture2D>();
    static List<Texture2D> listTextureRow = new List<Texture2D>();

    [SerializeField] private Camera screenCamera;
    [SerializeField] private Camera markerCamera;
    [SerializeField] private Camera mk1Camera;
    [SerializeField] private Camera mk2Camera;


    private static Texture2D mk2Texture;
    private static Texture2D m_ScreenTexture;
    private static Color[] m_ColorsArray;

    private int CountEndThread = 0;

    private static int m_CountWitePoints = 0;
    private static int m_CountFillArea = 0;
    private static int m_CountPointMk1 = 0;

 

    private void Start()
    {
        for (int i = 0; i < columnCamera.Count; i++)
        {
            renderColumn.Add(new RenderTexture(540, 960, 1));
            columnCamera[i].targetTexture = renderColumn[i];
        }
        for (int i = 0; i < rowCamera.Count; i++)
        {
            renderRow.Add(new RenderTexture(540, 960, 1));
            rowCamera[i].targetTexture = renderRow[i];
        }

        renderScreen = new RenderTexture(540, 960, 1);
        screenCamera.targetTexture = renderScreen;
        background.texture = renderScreen;

        //renderMarker = new RenderTexture(540, 960, 1);
        //markerCamera.targetTexture = renderMarker;

        renderMk1 = new RenderTexture(540, 960, 1);
        mk1Camera.targetTexture = renderMk1;
        renderMk2 = new RenderTexture(540, 960, 1);
        mk2Camera.targetTexture = renderMk2;
    }

    Texture2D ToTexture2D(RenderTexture rTex)
    {
        Texture2D tex = new Texture2D(rTex.width, rTex.height, TextureFormat.RGB24, false);
        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
        return tex;
    }

    void Enter()
    {
        mk2Texture = ToTexture2D(renderMk2);
        m_CountFillArea = 0;
        m_CountWitePoints = 0;
        Texture2D texture2D = ToTexture2D(renderScreen);
        m_ScreenTexture = texture2D;
        listTextureColumn.Clear();
        for (int i = 0; i < renderColumn.Count; i++)
        {
            listTextureColumn.Add(ToTexture2D(renderColumn[i]));
        }
        listTextureRow.Clear();
        for (int i = 0; i < renderRow.Count; i++)
        {
            listTextureRow.Add(ToTexture2D(renderRow[i]));
        }
        Debug.Log("textureColumn: " + listTextureColumn.Count);
        Debug.Log("textureRow: " + listTextureRow.Count);

        // countWhite = 0;
        //    matrixImage = ConverToMatrix(screenTexture.width, screenTexture.height, colors);


        //screenTexture.SetPixels(colors);
        //screenTexture.Apply();

        //background.texture = screenTexture;

        //widthImage = screenTexture.width;
        //heightImage = screenTexture.height;


        //Debug.Log("widthImage: " + matrixImage.GetLength(0));
        //Debug.Log("heightImage: " + matrixImage.GetLength(1));



        //Debug.Log("countOhterPoint: " + countOhterPoint);
        //Debug.Log("countWhite: " + countWhite);
        //Debug.Log("countAllMaskpoint: " + countAllMaskpoint);
        //float coefficient = (countAllMaskpoint / 50f);
        //Debug.Log("Coefficient: " + coefficient);
        //CountFill = (int)(countOhterPoint / coefficient);
        //Debug.Log("CountOhterPoint*Coefficient: "+ CountFill);




        //  matrixImage = ConverToMatrix(widthImage, heightImage, colors);

        StartCoroutine(WaitEndSearch());
    }

    static Color[] m_ArrayColumn;
    static Color[] m_ArrayRow;
    void CheckMask()
    {

        Color[] arrayMk1 = ToTexture2D(renderMk1).GetPixels();
        Color[] mk2maskArray = mk2Texture.GetPixels();
        int countWhite = 0;

        m_CountPointMk1 = 0;
        m_ColorsArray = m_ScreenTexture.GetPixels();
        for (int i = 0; i < arrayMk1.Length; i++)
        {
            if (arrayMk1[i] == Color.white)
            {
                m_CountPointMk1 += 1;
            }
        }

        for (int i = 0; i < m_ColorsArray.Length; i++)
        {
            if (mk2maskArray[i] != Color.black)
            {
                if (AverageSum(m_ColorsArray[i]) > 0.8f | IsRed(m_ColorsArray[i]))
                {
                    countWhite += 1;
                    if (countWhite >= (int)(m_CountPointMk1 * 0.9f))
                    { m_CountWitePoints += 1; countWhite = 0; }
                }
            }
        }

        for (int i = 0; i < listTextureColumn.Count; i++)
        {
            m_ArrayColumn = listTextureColumn[i].GetPixels();
            for (int j = 0; j < listTextureRow.Count; j++)
            {
                m_ArrayRow = listTextureRow[j].GetPixels();
                StartCoroutine(WaitThread(new Thread(CheckArray)));
            }
        }
    }
    static void CheckArray()
    {
        Color[] columnMask = m_ArrayColumn;
        Color[] rowMask = m_ArrayRow;
        int countPoint = 0;
        for (int i = 0; i < m_ColorsArray.Length; i++)
        {
            if (columnMask[i] != Color.black & rowMask[i] != Color.black)
            {
                if (IsBlue(m_ColorsArray[i]))
                {
                    m_ColorsArray[i] = Color.black;
                }

                if (m_ColorsArray[i] != Color.black)
                {
                    countPoint += 1;
                    m_ColorsArray[i] = Color.green;
                    if (countPoint >= m_CountPointMk1 / 8)
                    { m_CountFillArea += 1; return; }
                }
            }
        }
    }

    void Out()
    {
        if (m_CountWitePoints > 2) m_CountWitePoints = 2;

        m_CountFillArea -= m_CountWitePoints;

        if (m_CountFillArea < 0) m_CountFillArea = 0;
        if (m_CountFillArea > 50) m_CountFillArea = 50;

        text.text = "( " + m_CountFillArea.ToString() + ") " + m_CountWitePoints;

        GetComponent<SaveAndBack>().SetResult = m_CountFillArea;
    }

    public void Shoot()
    {
        loadPanel.SetActive(true);
        Enter();
        CheckMask();
    }

    private static float AverageSum(Color color)
    {
        float rezult = color.r + color.g + color.b;
        rezult /= 3f;
        return rezult;
    }

    private static bool IsRed(Color color)
    {
        if (color.r > color.g & color.r > color.b)
        { return true; }
        return false;
    }

    private static bool IsGreen(Color color)
    {
        if (color.g > color.r & color.g > color.b)
        { return true; }
        return false;
    }

    private static bool IsBlue(Color color)
    {
        if (color.b > color.r & color.b > color.g)
        {
            return true;
        }
        return false;
    }

    IEnumerator WaitThread(Thread thread)
    {
        thread.Start();
        yield return new WaitUntil(() => thread.ThreadState == ThreadState.Stopped);
        CountEndThread += 1;
    }

    IEnumerator WaitEndSearch()
    {
        yield return new WaitUntil(() => CountEndThread == 50);
        CountEndThread = 0;
        loadPanel.SetActive(false);
        Out();
    }
}