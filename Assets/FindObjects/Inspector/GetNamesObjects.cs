﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[CreateAssetMenu]
public class GetNamesObjects : ScriptableObject
{
    private GameObject m_levelObjects;
    
    

    [ContextMenu("GetNamesObjectsInDebug")]
    public void GetNames()
    {
        m_levelObjects = GameObject.FindWithTag("SoughtObject");
        string filename = "Assets/" + m_levelObjects.name + ".txt";
        
        StreamWriter sw = new StreamWriter(filename);
        
        sw.WriteLine("Уровень: " + m_levelObjects.name);

        foreach (Transform task in m_levelObjects.transform)
        {
            var description = task.GetComponent<Description>().GetTaskDescription;
            sw.WriteLine();
            sw.WriteLine("Задача: " + description);
            sw.WriteLine();
            sw.WriteLine("Объекты:");
            var names = task.GetComponentsInChildren<RussianName>();
            foreach (var name in names)
            {
                sw.WriteLine(name.gameObject.name + " - " + name.GetRussianName);
            }
        }
        sw.Close();
    }

    public void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 60, 20), "Запись"))
        {
            GetNames();
        }
    }
}
