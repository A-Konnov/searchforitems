﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Sprites;

[CreateAssetMenu]
public class SpritesAdd : ScriptableObject
{
    [SerializeField] private Sprite[] m_sprites;

    [ContextMenu("SpritesAdd")]
    public void Add()
    {
        GameObject objects = GameObject.FindWithTag("SoughtObject");

        var sr = objects.GetComponentsInChildren<SpriteRenderer>();

        foreach (var obj in sr)
        {
            foreach (var sprite in m_sprites)
            {
                if (obj.name == sprite.name)
                {
                    var objSprite = obj.GetComponent<SpriteRenderer>();
                    objSprite.sprite = sprite;
                    break;
                }
            }
        }
    }
}
