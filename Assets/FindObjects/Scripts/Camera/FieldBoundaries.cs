﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FieldBoundaries : MonoBehaviour
{
    private Vector2 m_boundaries;
    
    //public Text m_text;


    public Vector2 GetBoundaries()
    {
        var x = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth, 0)).x;
        var y = Camera.main.ScreenToWorldPoint(new Vector2(0, Camera.main.pixelHeight)).y;
        m_boundaries.x = x;
        m_boundaries.y = y;
        //m_text.text = "" + m_boundaries;
        
        return m_boundaries;
    }

}
