﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Sprites;

public class AdaptCameraSize : MonoBehaviour
{
    [SerializeField] private SpriteRenderer m_backgroundVertical;
    [SerializeField] private SpriteRenderer m_backgroundHorizontal;
    private SpriteRenderer m_background;

    private void Awake()
    {
        //Screen.orientation = ScreenOrientation.LandscapeLeft;
        
        var aspect = Camera.main.aspect;
        if (aspect > 1.77f)
        {
            m_background = m_backgroundHorizontal;
            Destroy(m_backgroundVertical.gameObject);
        }
        else
        {
            m_background = m_backgroundVertical;
            Destroy(m_backgroundHorizontal.gameObject);
        }

        gameObject.transform.position = new Vector3(0, 0, -10);
        m_background.transform.position = Vector3.zero;

        var backgroundAspect = m_background.sprite.rect.width / m_background.sprite.rect.height;

        if (Camera.main.aspect > backgroundAspect)
        {
            var width = m_background.bounds.extents.x * 2;
            Camera.main.orthographicSize = width / (Camera.main.aspect * 2);
        }
        else
        {
            var heiht = m_background.bounds.extents.y;
            Camera.main.orthographicSize = heiht;
        }
    }
}
