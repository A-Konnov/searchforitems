﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamera : MonoBehaviour
{
    [Header("Scroll")] [SerializeField] private float m_moveSpeed = 0.1f;
    private Vector3 m_currVelocity;
    private Vector2 m_firstTapPositon;
    private Vector2 m_moveTapPosition;
    private Vector3 m_targetVelocity;

    [Header("Zoom")] [SerializeField] private float m_zoomSpeed = 0.5f;
    [SerializeField] private float m_limitApproximation = 2f;
    [SerializeField] private bool m_invertZoom = true;
    private float m_limitEstrangement;
    private Vector3 m_newPosition;
    private float m_prevSize;
    private Vector3 m_touchCenterPosition;

    private Vector2 m_boundaries = Vector2.zero;
    private Camera m_camera;

    private void Start()
    {
        m_boundaries = GetComponent<FieldBoundaries>().GetBoundaries();
        m_camera = Camera.main;
        m_limitEstrangement = m_camera.orthographicSize;
    }

//    private void OnPreRender()
//    {
//        if (m_boundaries == Vector2.zero)
//        {
//            m_boundaries = GetComponent<FieldBoundaries>().GetBoundaries();
//        }
//    }

    private void Update()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    Application.targetFrameRate = 60;
                    m_firstTapPositon = m_camera.ScreenToWorldPoint(touch.position);
                    break;

                case TouchPhase.Moved:
                    m_moveTapPosition = m_camera.ScreenToWorldPoint(touch.position);
                    m_targetVelocity = m_firstTapPositon - m_moveTapPosition;
                    transform.position = transform.position + (m_targetVelocity * m_moveSpeed);
                    break;

                case TouchPhase.Ended:
                    Application.targetFrameRate = -1;
                    break;
            }
        }

        if (Input.touchCount == 2)
        {
            Touch touch0 = Input.GetTouch(0);
            Touch touch1 = Input.GetTouch(1);

            if (touch1.phase == TouchPhase.Began)
            {
                Application.targetFrameRate = 60;
                m_touchCenterPosition = CenterCamera(touch0, touch1);
            }

            if (touch1.phase == TouchPhase.Moved)
            {
                if (Zooming(touch0, touch1))
                {
                    m_newPosition = m_touchCenterPosition;
                    transform.position = Vector3.Lerp(transform.position, m_newPosition, 0.2f);
                }
            }

            if (touch0.phase == TouchPhase.Ended)
            {
                Application.targetFrameRate = -1;
                m_firstTapPositon = m_camera.ScreenToWorldPoint(touch1.position);
            }
            else if (touch1.phase == TouchPhase.Ended)
            {
                Application.targetFrameRate = -1;
                m_firstTapPositon = m_camera.ScreenToWorldPoint(touch0.position);
            }
        }

        CorrectPosition();
    }

    private void CorrectPosition()
    {
        var boundariesX = m_boundaries.x - (m_camera.aspect * m_camera.orthographicSize);
        var boundariesY = m_boundaries.y - m_camera.orthographicSize;

        var position = transform.position;

        position.x = Mathf.Clamp(position.x, boundariesX * -1, boundariesX);
        position.y = Mathf.Clamp(position.y, boundariesY * -1, boundariesY);
        position.z = -10f;

        transform.position = position;
    }

    private bool Zooming(Touch touch0, Touch touch1)
    {
        m_prevSize = m_camera.orthographicSize;

        Vector2 touch0PrevPos = touch0.position - touch0.deltaPosition;
        Vector2 touch1PrevPos = touch1.position - touch1.deltaPosition;

        var prevTouchDeltaMag = (touch0PrevPos - touch1PrevPos).magnitude;
        var touchDeltaMag = (touch0.position - touch1.position).magnitude;

        var increment = prevTouchDeltaMag - touchDeltaMag;

        increment = m_invertZoom ? increment * -1 : increment;

        m_camera.orthographicSize += increment * m_zoomSpeed * Time.deltaTime;
        m_camera.orthographicSize = Mathf.Clamp(m_camera.orthographicSize, m_limitApproximation, m_limitEstrangement);

        if (m_prevSize < Camera.main.orthographicSize)
        {
            return false;
        }

        return true;
    }

    private Vector3 CenterCamera(Touch touch0, Touch touch1)
    {
        Vector2 touch0Position = m_camera.ScreenToWorldPoint(touch0.position);
        Vector2 touch1Position = m_camera.ScreenToWorldPoint(touch1.position);

        Vector3 newPosition = touch1Position + ((touch0Position - touch1Position) * 0.5f);
        newPosition.z = -10;

        return newPosition;
    }
}