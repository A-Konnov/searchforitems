﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugFPS : MonoBehaviour
{
    private Text m_text;
    private float deltaTime = 0f;

    private void Start()
    {
        QualitySettings.vSyncCount = 0;
        m_text = GetComponent<Text>();
    }

    private void Update()
    {
        //Debug.Log(QualitySettings.vSyncCount);
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
        m_text.text = "" + Application.targetFrameRate + "  fps:" + 1f / deltaTime;
    }
}
