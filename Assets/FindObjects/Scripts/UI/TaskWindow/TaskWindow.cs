﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class TaskWindow : MonoBehaviour
{
    [SerializeField] private GenerateTargetsInTask m_generateTargetsInTask;
    [SerializeField] private TextMeshProUGUI m_taskText;

    private void OnEnable()
    {
        Time.timeScale = 0;
    }

    private void Start()
    {
        m_taskText.text = m_generateTargetsInTask.GetTaskDescription;
    }

    public void OnStartBtn()
    {
        Time.timeScale = 1;
    }

    public void Home()
    {
        SceneManager.LoadSceneAsync(0);
    }
}
