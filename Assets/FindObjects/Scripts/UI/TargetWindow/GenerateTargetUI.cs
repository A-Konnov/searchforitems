﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GenerateTargetUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_textUI;
    [SerializeField] private Image m_image;
    public void SetTargetInfo(GameObject target)
    {
        var rusName = target.GetComponent<RussianName>();

        if (target.CompareTag("ManyGoals"))
        {
            m_textUI.text = string.Format("Найдите все {0}, осталось {1}", rusName.GetRussianName, target.transform.childCount);
            m_image.sprite = target.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite;
        }
        else
        {
            m_textUI.text = string.Format("Найдите {0}", rusName.GetRussianName);
            m_image.sprite = target.GetComponent<SpriteRenderer>().sprite;
        }
    }

    public void SetCount(GameObject parent, int count)
    {
        var rusName = parent.GetComponent<RussianName>();

        m_textUI.text = string.Format("Найдите все {0}, осталось {1}", rusName.GetRussianName, count);
        m_image.sprite = parent.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite;
    }
}
