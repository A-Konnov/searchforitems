﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AnimationTargetUI : MonoBehaviour
{
    [SerializeField] private RectTransform m_findObjectWin;
    [SerializeField] private RectTransform m_helpBtn;
    private GenerateTargetUI m_generateTargetUI;

    private void Start()
    {
        m_generateTargetUI = GetComponent<GenerateTargetUI>();
    }

    public void OpenWindow()
    {
        m_findObjectWin.DOAnchorPosX(-5, 0.5f);
        m_helpBtn.DOAnchorPosX(60, 0.5f);
    }

    public void CloseWindow()
    {
        m_findObjectWin.DOAnchorPosX(420, 0.5f);
        m_helpBtn.DOAnchorPosX(-5, 0.5f);
    }

    public void ResetWindow(GameObject target)
    {
        Sequence seq = DOTween.Sequence();
        seq.Append(m_findObjectWin.DOAnchorPosX(420, 0.5f));
        seq.Join(m_helpBtn.DOAnchorPosX(-5, 0.5f));

        seq.AppendCallback(() => m_generateTargetUI.SetTargetInfo(target));

        seq.Append(m_findObjectWin.DOAnchorPosX(-5, 0.5f));
        seq.Join(m_helpBtn.DOAnchorPosX(60, 0.5f));

        seq.Play();
    }

    public void ResetCount(GameObject parent, int count)
    {
        Sequence seq = DOTween.Sequence();
        seq.Append(m_findObjectWin.DOAnchorPosX(420, 0.5f));
        seq.Join(m_helpBtn.DOAnchorPosX(-5, 0.5f));

        seq.AppendCallback(() => m_generateTargetUI.SetCount(parent, count));

        seq.Append(m_findObjectWin.DOAnchorPosX(-5, 0.5f));
        seq.Join(m_helpBtn.DOAnchorPosX(60, 0.5f));

        seq.Play();
    }
}
