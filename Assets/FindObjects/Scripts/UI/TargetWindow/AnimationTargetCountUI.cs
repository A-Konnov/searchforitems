﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class AnimationTargetCountUI : MonoBehaviour
{
    [SerializeField] private RectTransform m_countTargetWin;
    [SerializeField] private TextMeshProUGUI m_countTMP;

    public void OpenCountTarget(int count)
    {
        Sequence m_seq = DOTween.Sequence();
        m_seq.AppendCallback(() => SetCountTargets(count));
        m_seq.Append(m_countTargetWin.DOAnchorPosX(0, 0.5f));
        m_seq.Play();
    }

    public void SetCountTargets(int count)
    {
        m_countTMP.text = "" + count;
    }

    public void RewriteCountTargets(int count)
    {
        if (count > 0)
        {
            Sequence m_seq = DOTween.Sequence();
            m_seq.Append(m_countTargetWin.DOAnchorPosX(150, 0.5f));
            m_seq.AppendCallback(() => SetCountTargets(count));
            m_seq.Append(m_countTargetWin.DOAnchorPosX(0, 0.5f));
            m_seq.Play();
        }
        else
        {
            Sequence m_seq = DOTween.Sequence();
            m_seq.Append(m_countTargetWin.DOAnchorPosX(150, 0.5f));
            m_seq.Play();
        }
    }
}
