﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndWindow : MonoBehaviour
{
    [SerializeField] private ParticleSystem m_fx;
    
    public void EndLevel()
    {
        m_fx.Play(true);
        GetComponent<Animator>().SetTrigger("Open");
    }

    public void OnExit()
    {
        GetComponent<Animator>().SetTrigger("Home");
    }
}
