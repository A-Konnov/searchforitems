﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestStickersReplace : MonoBehaviour
{
    [SerializeField] private InputField m_inputField;
    private int m_result = 0;
    
    public void Test()
    {
        if (!int.TryParse(m_inputField.text, out m_result))
        {
            Debug.Log("Enter test result");
            return;
        }
            
        m_result = int.Parse(m_inputField.text);
        PlayerPrefs.SetInt("Stickers", m_result);
        Debug.Log("Stickers " + m_result);
    }
}
