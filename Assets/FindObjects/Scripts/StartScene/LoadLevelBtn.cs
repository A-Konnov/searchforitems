﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadLevelBtn : MonoBehaviour
{
    [SerializeField] private Animator m_animatorFinishTasksWindowUI;
    [SerializeField] private LevelsDB m_levelsDB;
    [SerializeField] private Image[] m_starsImg;
    [SerializeField] private GameObject m_blockPanel;
    
    private int m_levelInDB;
    public int SetLevelInDB
    {
        set => m_levelInDB = value;
    }

    public int GetLoadSceneNumber => m_levelInDB;

    public void OnLoadScene()
    {
        if (m_levelsDB.GetTaskCompleted(m_levelInDB) >= m_starsImg.Length)
        {
            Debug.Log("All mission finished");
            m_animatorFinishTasksWindowUI.SetTrigger("Open");
            return;
        }

        PlayerPrefs.SetInt("tasksCompleted", m_levelsDB.GetTaskCompleted(m_levelInDB));
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        SceneManager.LoadSceneAsync(m_levelsDB.GetNumberScene(m_levelInDB));
    }

    public void GenerateLevelBtn(int stars)
    {
        for (int i = 0; i < stars; i++)
        {
            m_starsImg[i].color = Color.yellow;
        }
    }

    public void ActivateBtn(bool active)
    {
        m_blockPanel.SetActive(!active);
    }
}
