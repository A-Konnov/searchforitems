﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StickersCount : MonoBehaviour
{
    private void Start()
    {
        GetComponent<Text>().text = ("Стикеров: " + PlayerPrefs.GetInt("Stickers"));
    }
}
