﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelsPlacement : MonoBehaviour
{
    [SerializeField] private Transform m_levelsWindow;
    [SerializeField] private LevelsDB m_levelsDB;
    private LoadLevelBtn[] m_loadLevelsBtn;

    private void Start()
    {
        m_loadLevelsBtn = m_levelsWindow.GetComponentsInChildren<LoadLevelBtn>();

        m_levelsDB.LoadDB(m_levelsWindow.childCount);

        for (int i = 0; i < m_loadLevelsBtn.Length; i++)
        {
            m_loadLevelsBtn[i].SetLevelInDB = i;
            m_loadLevelsBtn[i].GenerateLevelBtn(m_levelsDB.GetTaskCompleted(i));
        }

        InclusionLevels();
    }

    private void InclusionLevels()
    {
        float stickers = PlayerPrefs.GetInt("Stickers");
        Debug.Log(stickers);
        int enableLevels = (int) Math.Floor(stickers / 3);
        Debug.Log(enableLevels);

        if (enableLevels <= m_loadLevelsBtn.Length)
        {
            for (int i = 0; i < m_loadLevelsBtn.Length; i++)
            {
                if (i >= enableLevels)
                {
                    return;
                }
                
                if (m_levelsDB.GetTaskCompleted(i) < 4)
                {
                    m_loadLevelsBtn[i].ActivateBtn(true);
                }
            }
            return;
        }

        enableLevels = Mathf.Clamp((enableLevels - m_loadLevelsBtn.Length), 0, 7);

        for (int i = 0; i < m_loadLevelsBtn.Length; i++)
        {
            if (m_levelsDB.GetTaskCompleted(i) >= 4)
            {
                if (i <= enableLevels)
                {
                    m_loadLevelsBtn[i].ActivateBtn(true);
                }
            }
            else
            {
                m_loadLevelsBtn[i].ActivateBtn(true);
            }
        }
    }
}