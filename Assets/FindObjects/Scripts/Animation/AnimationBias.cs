﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AnimationBias : MonoBehaviour
{
    [SerializeField] private Vector2 m_move = new Vector2(0.5f,0);
    [SerializeField] private float m_speed = 0.5f;

    private Vector2 m_startPoint;

    private void Start()
    {
        m_startPoint = gameObject.transform.position;
        var position = (Vector2)gameObject.transform.position + m_move;

        gameObject.transform.DOMove(position, m_speed).SetLoops(-1, LoopType.Yoyo).SetSpeedBased(true).SetEase(Ease.InOutQuad);
    }

    private void OnDrawGizmosSelected()
    {
        var startPoint = gameObject.transform.position;

        if (Application.isPlaying)
        {
            startPoint = m_startPoint;
        }

        var start = (Vector2)startPoint;
        var end = (Vector2)startPoint + m_move;
        Gizmos.DrawSphere(start, 0.2f);
        Gizmos.DrawSphere(end, 0.2f);
        Gizmos.DrawLine(start, end);
    }
}
