﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Serialization;

public class AnimationWiggle : MonoBehaviour
{
    [SerializeField] private float m_angleWiggle = 5f;
    [SerializeField] private float m_timeRotate = 1f;
    private void Start()
    {
        gameObject.transform.DOLocalRotate(new Vector3(0, 0, m_angleWiggle), m_timeRotate, RotateMode.LocalAxisAdd).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutQuad);
    }
}
