﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Sprites;

public class AnimationDetectTarget : MonoBehaviour
{
    private SpriteRenderer m_sr;
    [SerializeField] private ParticleSystem m_fx;
    private Vector3 m_center = Vector3.zero;

    public void PlayAnimation(GameObject target)
    {
        m_fx.gameObject.transform.position = target.transform.position;
        m_fx.Play(true);
        
        m_sr = target.GetComponent<SpriteRenderer>();
        if (m_sr != null)
        {
            m_sr.sortingLayerName = "Animation";
            m_center = m_sr.sprite.bounds.center;
        }

        Sequence sequence = DOTween.Sequence();

        sequence.Append(target.transform.DOScale(new Vector3(2, 2, 0), 0.5f));
        sequence.Join(transform.DOMove((target.transform.position - m_center), 0.5f));

        if (m_sr != null)
        {
            sequence.Join(m_sr.DOFade(0, 1));
        }

        if (target.transform.childCount > 0)
        {
            foreach (Transform obj in target.transform)
            {
                var objSr = obj.GetComponent<SpriteRenderer>();
                if (objSr != null)
                {
                    objSr.sortingLayerName = "Animation";
                    sequence.Join(objSr.DOFade(0, 1));
                }
            }
        }

        sequence.Play();
    }
}
