﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Description : MonoBehaviour
{
    [SerializeField] private string m_taskDescription;
    [SerializeField] private bool m_taskDone;

    public string GetTaskDescription => m_taskDescription;
    public bool TaskDone
    {
        get => m_taskDone;
        set => m_taskDone = value;
    }

}
