﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class BlockTouch : MonoBehaviour
{
    [SerializeField] private Image m_blockPanelUI;
    [SerializeField] private int m_touchCountForBlock = 8;
    [SerializeField] private float m_timeTouchDown = 1f;
    [SerializeField] private float m_blockTime = 3f;
    private int m_curTouchCount;
    private float m_curTimeTouchDown;
    private Sequence m_seq;
   

    private void Start()
    {
        GetComponent<DetectTarget>().Detect.AddListener(ResetTouchs);
        m_blockPanelUI.gameObject.SetActive(false);
        m_curTouchCount = 0;
    }

    private void Update()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (Input.GetMouseButtonDown(0))
            {
                m_curTimeTouchDown = 0;
            }

            if (Input.GetMouseButton(0))
            {
                m_curTimeTouchDown += Time.deltaTime;
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (m_curTimeTouchDown <= m_timeTouchDown)
                {
                    m_curTouchCount++;
                }
                else
                {
                    m_curTouchCount = 0;
                }

                m_curTimeTouchDown = 0;

                if (m_curTouchCount >= m_touchCountForBlock)
                {
                    m_seq = DOTween.Sequence();

                    m_seq.AppendCallback(SwitchActiveBlockPanel);
                    m_seq.Append(m_blockPanelUI.DOFade(0.5f, 0.2f));
                    m_seq.AppendInterval(m_blockTime);
                    m_seq.Append(m_blockPanelUI.DOFade(0, 1f));
                    m_seq.AppendCallback(SwitchActiveBlockPanel);
                    m_seq.Play();

                    m_curTouchCount = 0;
                }
            }
        }
    }

    private void ResetTouchs()
    {
        m_curTouchCount = 0;
    }

    private void OnDestroy()
    {
        GetComponent<DetectTarget>().Detect.RemoveListener(ResetTouchs);
    }

    private void SwitchActiveBlockPanel()
    {
        if (m_blockPanelUI.gameObject.activeSelf)
        {
            m_blockPanelUI.gameObject.SetActive(false);
        }
        else
        {
            m_blockPanelUI.gameObject.SetActive(true);
        }
    }
}
