﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GenerateTarget : MonoBehaviour
{
    private List<GameObject> m_targets = new List<GameObject>();
    private int m_multiTarget = 0;

    [Header("UI:")]
    [SerializeField] private AnimationTargetCountUI m_animationTargetCountUI; //no use
    [SerializeField] private AnimationTargetUI m_animationTargetUI;
    [SerializeField] private EndWindow m_endWindowUI;
    [SerializeField] private RectTransform m_findObjectWin;
    [SerializeField] private RectTransform m_helpBtn;
    
    public UnityEvent Win = new UnityEvent();

    private void Start()
    {
        m_targets = GetComponent<GenerateTargetsInTask>().GetTargets;

        GetTarget();
    }

    private void GetTarget()
    {
        if (m_targets.Count <= 0)
        {
            LevelEnd();
            return;
        }

        var index = UnityEngine.Random.Range(0, m_targets.Count);

        if (m_targets[index].CompareTag("ManyGoals"))
        {
            m_multiTarget = m_targets[index].transform.childCount;

            foreach (Transform target in m_targets[index].transform)
            {
                ActivateTarget(target.gameObject);
            }
        }
        else
        {
            ActivateTarget(m_targets[index]);
        }

        m_animationTargetUI.ResetWindow(m_targets[index]);
    }

    private void LevelEnd()
    {
        Win.Invoke();
        m_animationTargetUI.CloseWindow();
        GetComponent<UpdateResult>().UpdateDB();
        m_endWindowUI.EndLevel();
    }

    public void CheckTarget(GameObject obj)
    {
        if (m_multiTarget == 0)
        {
            DeactivateTarget(obj);
            m_targets.Remove(obj);
            GetTarget();
            return;
        }

        if (m_multiTarget > 1)
        {
            DeactivateTarget(obj);
            m_multiTarget--;
            m_animationTargetUI.ResetCount(obj.transform.parent.gameObject, m_multiTarget);
        }
        else
        {
            DeactivateTarget(obj);
            m_targets.Remove(obj.transform.parent.gameObject);
            m_multiTarget = 0;
            GetTarget();
        }
    }

    private void ActivateTarget(GameObject obj)
    {
        obj.AddComponent<CircleCollider2D>();
        obj.AddComponent<Enlarge>();
    }

    private void DeactivateTarget(GameObject obj)
    {
        Destroy(obj.GetComponent<Collider2D>());
        Destroy(obj.GetComponent<Enlarge>());
    }
}
