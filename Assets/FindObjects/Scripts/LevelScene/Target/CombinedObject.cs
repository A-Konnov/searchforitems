﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombinedObject : MonoBehaviour
{
    [SerializeField] private GameObject[] m_attachedObjects;

    public void Join()
    {
        foreach (var obj in m_attachedObjects)
        {
            obj.transform.parent = gameObject.transform;
        }
    }
}
