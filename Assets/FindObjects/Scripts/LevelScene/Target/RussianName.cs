﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RussianName : MonoBehaviour
{
    [SerializeField] private string m_russianName;

    public string GetRussianName
    {
        get { return m_russianName; }
    }

}
