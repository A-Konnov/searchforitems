﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GenerateTargetsInTask : MonoBehaviour
{
    [SerializeField] private GameObject[] m_tasks;
    private List<GameObject> m_targets = new List<GameObject>();
    public List<GameObject> GetTargets => m_targets;

    private string m_taskDescription;
    public string GetTaskDescription => m_taskDescription;

    private int m_taskNumber;
    public int GetTaskNumber => m_taskNumber;


    private void Awake()
    {
        Debug.Log("efewfe");
        m_taskNumber = PlayerPrefs.GetInt("tasksCompleted");

        m_taskDescription = m_tasks[m_taskNumber].GetComponent<Description>().GetTaskDescription;

        foreach (Transform target in m_tasks[m_taskNumber].transform)
        {
            m_targets.Add(target.gameObject);
        }

        var combinedObjects = m_tasks[m_taskNumber].GetComponentsInChildren<CombinedObject>();
        if (combinedObjects.Length > 0)
        {
            foreach (var combined in combinedObjects)
            {
                combined.Join();
            }
        }
    }
}