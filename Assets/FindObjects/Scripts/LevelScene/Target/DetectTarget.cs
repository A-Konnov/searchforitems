﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class DetectTarget : MonoBehaviour
{
    private GenerateTarget m_generateTarget;
    private AnimationDetectTarget m_animationDetectTarget;

    public UnityEvent Detect = new UnityEvent();

    private void Start()
    {
        m_generateTarget = GetComponent<GenerateTarget>();
        m_animationDetectTarget = GetComponent<AnimationDetectTarget>();
    }

    private void Update()
    {
        if (!EventSystem.current.IsPointerOverGameObject() && Input.GetMouseButtonDown(0))
        {
            var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector3.forward);
            if (hit.collider != null)
            {
                m_generateTarget.CheckTarget(hit.collider.gameObject);
                m_animationDetectTarget.PlayAnimation(hit.collider.gameObject);
                Detect.Invoke();
            }
        }
    }
}
