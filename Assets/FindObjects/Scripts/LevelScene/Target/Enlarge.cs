﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Enlarge : MonoBehaviour
{
    private EnlargeSetup m_ES;
    private WaitForSeconds m_delay;
    private Tween m_anim;
    private Vector3 m_scale;

    private void Start()
    {
        m_ES = Resources.Load("EnlargeSetup") as EnlargeSetup;
        m_delay = new WaitForSeconds(m_ES.GetTimeDelay);
        m_scale = transform.localScale;
        StartCoroutine(PunchScale());
    }

    IEnumerator PunchScale()
    {
        yield return m_delay;

        if (gameObject.transform.childCount == 0)
        {
            //gameObject.transform.DOPunchScale(m_ES.GetScale, m_ES.GetDuration, m_ES.GetAmount);
            m_anim = transform.DOScale(m_ES.GetScale, m_ES.GetDuration);
        }
        else
        {
            List<Transform> childs = new List<Transform>();
            foreach (Transform child in gameObject.transform)
            {
                if (child.gameObject.activeSelf)
                {
                    childs.Add(child);
                }
            }

            int random = UnityEngine.Random.Range(0, childs.Count);
            m_anim = childs[random].DOScale(m_ES.GetScale, m_ES.GetDuration).SetLoops(-1, LoopType.Yoyo);
        }
        m_anim.Play().SetLoops(-1, LoopType.Yoyo);
    }

    private void OnDestroy()
    {
        m_anim.Kill();
        transform.localScale = m_scale;
    }
}
