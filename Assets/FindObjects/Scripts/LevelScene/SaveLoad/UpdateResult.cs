﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UpdateResult : MonoBehaviour
{
    [SerializeField] private LevelsDB m_levelDB;

    public void UpdateDB()
    {
        var levelIndex = SceneManager.GetActiveScene().buildIndex - 1;
        var tasksCompleted = m_levelDB.GetTaskCompleted(levelIndex);
        tasksCompleted++;
        m_levelDB.ReplaceTaskInLevel(levelIndex, tasksCompleted);
    }
}
