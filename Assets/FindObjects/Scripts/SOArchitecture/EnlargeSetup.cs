﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnlargeSetup : ScriptableObject
{
    [SerializeField] private Vector3 m_scale;
    [SerializeField] private float m_duration;
    [SerializeField] private int m_amount;
    [SerializeField] private float m_timeDelay;

    public Vector3 GetScale { get => m_scale; }
    public float GetDuration { get => m_duration; }
    public int GetAmount { get => m_amount; }
    public float GetTimeDelay { get => m_timeDelay; }
}
