﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[CreateAssetMenu]
public class LevelsDB : ScriptableObject
{
    [Serializable]
    public struct Level
    {
        public int m_numberScene;
        public int m_tasksCompleted;
    }

    [SerializeField] private List<Level> m_levels;

    public List<Level> Levels
    {
        get { return m_levels; }
        set { m_levels = value; }
    }

    public void ReplaceTaskInLevel(int index, int taskComplete)
    {
        var level = m_levels[index];
        level.m_tasksCompleted = taskComplete;
        m_levels[index] = level;
        Save(m_levels);
    }

    public int GetTaskCompleted(int index)
    {
        return m_levels[index].m_tasksCompleted;
    }

    public int GetNumberScene(int index)
    {
        return m_levels[index].m_numberScene;
    }

    public void LoadDB(int levelsCouont)
    {
        m_levels = Load(ref m_levels);

        if (m_levels == null)
        {
            m_levels = new List<Level>();
            for (int i = 0; i < levelsCouont; i++)
            {
                Level level = new Level();
                level.m_numberScene = i + 1;
                level.m_tasksCompleted = 0;
                m_levels.Add(level);
            }
            Save(m_levels);
        }
    }

    private string GetDataPath()
    {
        string folderPath = Path.Combine(Application.persistentDataPath, "BinarySave");

        if (!Directory.Exists(folderPath))
            Directory.CreateDirectory(folderPath);

        string dataPath = Path.Combine(folderPath, "SaveLevels.bs");
        //C:/Users/konnov/AppData/LocalLow/Asterman/SearchForItems\BinarySave\
        return dataPath;
    }

    private T Load<T>(ref T obj)
    {
        string dataPath = GetDataPath();

        if (File.Exists(dataPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(dataPath, FileMode.Open);
            obj = (T)bf.Deserialize(file);
            file.Close();
            return obj;
        }

        Debug.Log("no file");
        return default(T);
    }

    private void Save(object obj)
    {
        string dataPath = GetDataPath();

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(dataPath, FileMode.OpenOrCreate);
        bf.Serialize(file, obj);
        file.Close();
    }

    [ContextMenu("Save")]
    public void SaveInInspector()
    {
        Save(m_levels);
    }


}
