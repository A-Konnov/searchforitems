Уровень: Fight

Задача: Поборимся?

Объекты:
Backpack - рюкзак
Balls - мячи
Bandage - бинт
Coblet - кубок
Flag - флаг
Glowes - перчатки
Horse_sport - атлетического коня
Medal - медаль
Medicine - аптечку
Oil - банку масла
Rackets - рокетки
Running_shoe - кроссовки
Scarf - шарф
Scooter - самокат
Sport_bag - спортивную сумку
Sport_running - беговую дорожку
Trainer - тренажёр
Volleyball_ball - волейбольный мяч
Weight - гири
Winner_place - призовой подиум

Задача: Животные и растения

Объекты:
Birds - птицы
Cherry - вишенку
Dog - собаку
Dragon - дракона
Eagle - орла
Flower - цветок
Frog - лягушку
Goat - горного козла
Ladybug - божью коровку
Mushrooms - грибы
Palm - пальму
Parsley - петрушку
Paspberry - малину
Pingeon - голубя
Rabbit - кролика
Shell - ракушку
Snail - улитку
Unicorn - единорога

Задача: Технический прогресс

Объекты:
Balloons - воздушные шары
Bench - скамейку
Bicycle - велосипед
Car - автомобиль
Chairs - стулья
Fountain - фонтан
Fridge - переносной холодильник
High - опоры ЛЭП
Houses - домики
Icecream_track - фургон мороженщика
Light - фонарик
Plane - самолёт
Record - радиоприёмник
Train - паровоз

Задача: Маленькие археологи

Объекты:
Baloons - надувные шарики
Bone - косточку
Book - книгу
Box - сундук
Carpet - ковёр
Coins - монеты
Cup - кружку
Curcles_no_the_field - круг на поле
Easter_statue - статую острова Пасхи
Fire - факел
Helmet - шлем
Lion_statue - статую льва
Notebook - блокнот
Obelisk - обелиск
Pencile - карандаш
Tent - палатку
Tracks - следы
UFO - НЛО

Задача: Расскажем обо всём

Объекты:
Apple - яблоко
Bag - дипломат
Banana - банан
Board - доску
Bushes - кусты
Can - банку
Candies - конфеты
Envelope - письмо
Fork - вилку
Glasses - очки
Hot-dog - хот-дог
Knight - складной нож
Lake - озеро
Music_speaker - колонки
Pizza - пицу
Purse - кошелёк
Star - звезду
BeachUmbrella - пляжный зонт
Umbrella - зонтик
Water - бутылку воды
