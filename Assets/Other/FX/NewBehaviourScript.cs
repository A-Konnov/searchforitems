﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class NewBehaviourScript : MonoBehaviour
{
    [SerializeField] private ParticleSystem fx;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            fx.Play(true);
            Debug.Log("OK");
        }
    }
}
